# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=keysmith
pkgver=23.04.3
pkgrel=1
pkgdesc="OTP client for Plasma Mobile and Desktop"
url="https://invent.kde.org/kde/keysmith"
arch="all !armhf"
license="GPL-3.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	ki18n-dev
	kirigami2-dev
	libsodium-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/keysmith.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/keysmith-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a7ad96c91f5b50954aa185bdec81af8feafc1190105d28a3166b84442e5d58bd8a6dddcabd9d1f907b585f4082fe2dd16991b0c922bde087e1edfcc0b28027a1  keysmith-23.04.3.tar.xz
"
