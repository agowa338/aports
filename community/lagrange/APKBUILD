# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=lagrange
pkgver=1.16.6
pkgrel=0
pkgdesc="Beautiful Gemini client"
url="https://gmi.skyjake.fi/lagrange"
license="BSD-2-Clause"
arch="all"
makedepends="
	cmake
	fribidi-dev
	harfbuzz-dev
	libunistring-dev
	libwebp-dev
	mpg123-dev
	openssl-dev
	pcre2-dev
	samurai
	sdl2-dev
	zip
	zlib-dev
	"
subpackages="$pkgname-dbg $pkgname-doc"
source="https://git.skyjake.fi/gemini/lagrange/releases/download/v$pkgver/lagrange-$pkgver.tar.gz"
options="!check" # no test suite

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DENABLE_POPUP_MENUS=OFF \
		-DENABLE_RESIZE_DRAW=OFF \
		-DENABLE_X11_XLIB=OFF \
		-DTFDN_ENABLE_SSE41=OFF
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d26abdb01efa2109f97c2af23da567b6fbc4e279618efdd0f30547ae1c1048e0a10745360c51ee1dca008e6f73de53912ee0b672c7e905171a18c75286d315f0  lagrange-1.16.6.tar.gz
"
