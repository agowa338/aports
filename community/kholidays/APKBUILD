# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kholidays
pkgver=5.108.0
pkgrel=2
pkgdesc="Support for icon themes"
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
url="https://community.kde.org/Frameworks"
license="LGPL-2.0-or-later"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kholidays.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kholidays-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kholidays.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bbf5cb7b2205d57205d241d33f35c4f6c3b8c73c89c84ecb81b5be5473968eebb577182e8a0a73e5c8a41b1276a2ab2abd965e66dd3a5ee39ecc792d30d7368f  kholidays-5.108.0.tar.xz
"
