# Contributor:
# Maintainer: Steven Guikal <void@fluix.one>
pkgname=py3-sqlalchemy
pkgver=2.0.19
pkgrel=1
pkgdesc="object relational mapper for Python"
url="https://pypi.org/project/SQLAlchemy"
arch="all"
license="MIT"
depends="py3-greenlet"
makedepends="
	cython
	py3-gpep517
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="
	py3-mock
	py3-mypy
	py3-pytest
	py3-pytest-xdist
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/S/SQLAlchemy/SQLAlchemy-$pkgver.tar.gz"
builddir="$srcdir/SQLAlchemy-$pkgver"

replaces="py-sqlalchemy" # Backwards compatibility
provides="py-sqlalchemy=$pkgver-r$pkgrel" # Backwards compatibility

# crashloops until it eventually passes, fixme
case "$CARCH" in
x86) options="$options !check" ;;
esac

prepare() {
	default_prepare
	# we have a new enough python for every typing extension
	grep -l -r 'typing_extensions' . \
		| grep '.py' \
		| xargs -n 1 -P ${JOBS:-1} sed -i 's|typing_extensions|typing|g'
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d82834f4f55c2298ff16401f34326845360c58e6ba6a2e22cda92219c0fb40938d404315027f4e9f392bc57fe45808ea192f99b747e073a40a9713f8d498b22a  SQLAlchemy-2.0.19.tar.gz
"
