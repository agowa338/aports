# Contributor:
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=py3-faker
_pyname=Faker
pkgver=19.3.0
pkgrel=0
pkgdesc="Python package that generates fake data for you"
url="https://faker.readthedocs.io/en/master"
license="MIT"
arch="noarch"
depends="py3-dateutil"
makedepends="py3-setuptools"
checkdepends="py3-email-validator py3-ipaddress py3-mock py3-freezegun
	py3-more-itertools py3-pytest py3-ukpostcodeparser py3-validators
	py3-pytest-runner py3-random2 py3-pillow"
_pypiprefix="${_pyname%${_pyname#?}}"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/$_pypiprefix/$_pyname/$_pyname-$pkgver.tar.gz"
builddir=$srcdir/$_pyname-$pkgver

replaces="py-faker" # Backwards compatibility
provides="py-faker=$pkgver-r$pkgrel" # Backwards compatibility

case "$CARCH" in
	# blocked by py3-pillow
	s390x|riscv64) options="!check" ;;
esac

prepare() {
	default_prepare
	# tests erroneously require a specific version of pytest
	sed -i setup.py -e 's/ *"pytest>=.*//g'
}

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD" pytest
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
cfbb81d8751fbe1c90260f806f36a8c95e7bcfe5f30555bb168b80e5e438a4e427482eb4220b55a6bd0f04ce4d64e9bd24b757b852b5a22de5f2b127e3f3fe25  Faker-19.3.0.tar.gz
"
