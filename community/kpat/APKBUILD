# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kpat
pkgver=23.04.3
pkgrel=1
pkgdesc="KPatience offers a selection of solitaire card games"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/kpat/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="carddecks"
makedepends="
	black-hole-solver-dev
	extra-cmake-modules
	freecell-solver-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/games/kpat.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpat-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
# Broken tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
3802c83ac07658fb189c45b4865900bd19a800d9e1ce90e921083a2879b230ed57e71e313daf126eff5da8fd1c28c3dcc154dba3ffa86ff87c8d6cc9370fd4d8  kpat-23.04.3.tar.xz
"
