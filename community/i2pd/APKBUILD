# Maintainer: r4sas <r4sas@i2pmail.org>
# Contributor: l-n-s <supervillain@riseup.net>
pkgname=i2pd
pkgver=2.48.0
pkgrel=1
pkgdesc="I2P Router written in C++"
url="https://github.com/PurpleI2P/i2pd"
arch="all"
license="BSD-3-Clause"
pkgusers="i2pd"
depends="musl-utils"
makedepends="boost-dev miniupnpc-dev openssl-dev>3 openssl zlib-dev"
options="!check" # No test suite
install="$pkgname.pre-install"
source="https://github.com/PurpleI2P/i2pd/archive/$pkgver/i2pd-$pkgver.tar.gz
	i2pd.initd
	i2pd.confd
	"
subpackages="$pkgname-dbg $pkgname-openrc"

build() {
	local _arch_opts="USE_UPNP=yes"
	make $_arch_opts
}

package() {

	install -D -m 755 i2pd "$pkgdir"/usr/sbin/i2pd

	install -dm755 -o $pkgusers \
		"$pkgdir"/etc/$pkgname \
		"$pkgdir"/usr/share/$pkgname \
		"$pkgdir"/var/lib/$pkgname \
		"$pkgdir"/var/log/$pkgname

	install -D -m 644 contrib/tunnels.conf "$pkgdir"/etc/i2pd/tunnels.conf
	install -D -m 644 contrib/i2pd.conf "$pkgdir"/etc/i2pd/i2pd.conf
	cp -r contrib/certificates/ "$pkgdir"/usr/share/$pkgname/certificates
	ln -s /usr/share/$pkgname/certificates "$pkgdir"/var/lib/$pkgname/certificates

	install -m755 -D "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
	install -m644 -D "$srcdir"/$pkgname.confd \
		"$pkgdir"/etc/conf.d/$pkgname
}

sha512sums="
51c2057e96ec87ec0885cc6de4f4ff1d4d898ce0785f58c1a028677247743d44fa1980aa13b7746a0af76d600f2b4cc3bf3408bd199c82efcb432242f5b24b80  i2pd-2.48.0.tar.gz
fae08de6cbdb5097cb3068d9a87509195d607fe666db870a3264f952fb3fceafc665168bd16bf598597617e48322301bbc900dcbd2c38fefca419d4755cea5f9  i2pd.initd
5b767037b49a9d94ac12dcc014a34c63967ab16a3292dd2622a118326c8d54905213d9638e48903cf0115c69b37490f648d3b2a78a1099063af78178b7024c75  i2pd.confd
"
