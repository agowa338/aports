# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kpmcore
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/system/org.kde.partitionmanager"
pkgdesc="Library for managing partitions"
license="GPL-3.0-or-later"
depends="
	device-mapper-udev
	sfdisk
	smartmontools
	"
makedepends="
	extra-cmake-modules
	kauth-dev
	kcoreaddons-dev
	ki18n-dev
	kwidgetsaddons-dev
	qca-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/system/kpmcore.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpmcore-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires running dbus server

# secfixes:
#   4.2.0-r0:
#     - CVE-2020-27187

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6fa49f53cc92843b24af1cca52cfaf5096891a3cffa07d5167415fe624ed429aaa88981d340003e688400b3c954f1d927180f750250ed2b8e4c88010176f55b9  kpmcore-23.04.3.tar.xz
"
