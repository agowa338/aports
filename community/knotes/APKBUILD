# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=knotes
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# ppc64le and s390x blocked by kdepim-runtime
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
pkgdesc="Popup notes"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
depends="kdepim-runtime"
makedepends="
	akonadi-dev
	akonadi-notes-dev
	akonadi-search-dev
	extra-cmake-modules
	grantlee-dev
	grantleetheme-dev
	kcalutils-dev
	kcmutils-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdnssd-dev
	kdoctools-dev
	kglobalaccel-dev
	kiconthemes-dev
	kitemmodels-dev
	kitemviews-dev
	kmime-dev
	knewstuff-dev
	knotifications-dev
	knotifications-dev
	knotifyconfig-dev
	kontactinterface-dev
	kparts-dev
	kpimtextedit-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	libxslt-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/knotes.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/knotes-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# knotesgrantleeprinttest is broken
	xvfb-run ctest --test-dir build --output-on-failure -E "knotesgrantleeprinttest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
94868d19a535b79049a53503acc26b26ec0d94a4c527f95c439ac30874d71657c13fd0c7206f38b9dd669b25f42a231150d3aead15e3557767ef70dbd2637cfd  knotes-23.04.3.tar.xz
"
