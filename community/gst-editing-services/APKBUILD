# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=gst-editing-services
pkgver=1.22.5
pkgrel=0
pkgdesc="GStreamer Editing Services Library"
url="https://gstreamer.freedesktop.org"
# s390x blocked by 7 failing tests
arch="all !s390x"
license="LGPL-2.0-or-later"
makedepends="
	flex
	glib-dev
	gobject-introspection-dev
	gst-plugins-bad-dev
	gst-plugins-base-dev
	gst-plugins-good
	gstreamer-dev
	gtk-doc
	libxml2-dev
	meson
	py3-gobject3-dev
	python3
	"
subpackages="$pkgname-dev $pkgname-doc"
source="https://gstreamer.freedesktop.org/src/gst-editing-services/gst-editing-services-$pkgver.tar.xz"
options="!check" # https://gitlab.freedesktop.org/gstreamer/gst-editing-services/-/issues/125

build() {
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2" \
	CPPFLAGS="$CPPFLAGS -O2" \
	abuild-meson \
		-Db_lto=true \
		-Dtests="$(want_check && echo enabled || echo disabled)" \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
873bc9f9f4d4f7e2d94fc251d461080a56c0e34f44ee9b51a6f17a3ba64195a5ab2c8491705b1999acde555822c66e7ec5d3af548afe797c26bc7e14eec9abfd  gst-editing-services-1.22.5.tar.xz
"
