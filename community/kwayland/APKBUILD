# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kwayland
pkgver=5.108.0
pkgrel=2
pkgdesc="Qt-style Client and Server library wrapper for the Wayland libraries"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://www.kde.org"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	qt5-qtwayland-dev
	wayland-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	plasma-wayland-protocols
	qt5-qtbase-dev
	qt5-qttools-dev
	wayland-protocols
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/kwayland.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwayland-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-dbg"
options="!check" # Fails due to requiring running Wayland compositor

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kwayland.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
52aa73f2a36b7f1a17e32c5d8d8a3ebc94605d7c3bbab160ac7107f41d64b3c464a571197eab32cc20307d9564f9ae016daae124036b61708a91569ff7f1dcde  kwayland-5.108.0.tar.xz
"
