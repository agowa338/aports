# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=rocs
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# s390x blocked by kxmlgui
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/education/org.kde.rocs"
pkgdesc="Graph Theory IDE"
license="GPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only) AND GFDL-1.2-only"
makedepends="
	boost-dev
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdeclarative-dev
	kdoctools-dev
	ki18n-dev
	kitemviews-dev
	ktexteditor-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/education/rocs.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/rocs-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# TestTgfFileFormat, TestRocs1FileFormat, TestRocs2FileFormat, and
	# graphtheory-test_graphoperations are broken
	# TestProject requires OpenGL
	local skipped_tests="("
	local tests="
		TestTgfFileFormat
		TestRocs1FileFormat
		TestRocs2FileFormat
		graphtheory-test_graphoperations
		TestProject"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)"
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
293c2c3458b9ac0d9e6abd8be86c6df7465365b72b8cb9f12dfe08fbef696621eda92925e5dd30f3326e86c1b290371274af046585489c49a5e72de9d184ed09  rocs-23.04.3.tar.xz
"
