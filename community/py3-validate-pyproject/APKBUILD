# Maintainer:
pkgname=py3-validate-pyproject
pkgver=0.13
pkgrel=1
pkgdesc="Validation library for simple check on pyproject.toml"
url="https://validate-pyproject.readthedocs.io"
arch="noarch"
license="MPL-2.0"
depends="
	py3-fastjsonschema
	python3
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="py3-pytest-xdist"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver-2.tar.gz::https://github.com/abravalheri/validate-pyproject/archive/refs/tags/v$pkgver.tar.gz
	no-useless-check.patch
	"
builddir="$srcdir/validate-pyproject-$pkgver"

build() {
	gpep517 build-wheel	\
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -k 'not test_downloaded and not test_private_classifier'
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
d0bd32b081f8890a2006d2ea74da31c29b5826f2dbc742f4cf5e5ddd10e67f8e6156026f7dab9c547a5c5c44d0e16d4eb4f11a49a1d17797f4b2a7c8c6f54612  py3-validate-pyproject-0.13-2.tar.gz
2f3e5dd1457fe81eb863ac0a53d2bb1ec991e3ac874f5210c53c19b0bb22b5bdf82b98d6107e1866a8b0c787326b457fea1151a6d3a11974248597d061a359c5  no-useless-check.patch
"
