# Contributor: jane400 <ralfrachinger@gmail.com>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=plasmatube
pkgver=23.04.3
pkgrel=1
pkgdesc="Kirigami YouTube video player based on Invidious"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma-mobile/plasmatube"
license="GPL-3.0-or-later AND CC0-1.0"
depends="
	gst-libav
	gst-plugins-good
	gst-plugins-good-qt
	kcoreaddons
	kdeclarative
	kirigami2
	qt5-qtquickcontrols2
	yt-dlp
	"
makedepends="
	extra-cmake-modules
	ki18n-dev
	kirigami2-dev
	mpv-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/multimedia/plasmatube.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/plasmatube-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
29f90afa762d6a1873e07b9af7dbaaf308c9213194304b4cee03cb4dcb6d97fcab7bfc2e95cd114eb81a59e677e0dd6947fca1feb30bb2ec490fde29097d1727  plasmatube-23.04.3.tar.xz
"
