# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kscreen
pkgver=5.27.7
pkgrel=1
pkgdesc="KDE's screen management software"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="hicolor-icon-theme"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	kdeclarative-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	layer-shell-qt-dev
	libkscreen-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsensors-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kscreen.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kscreen-$pkgver.tar.xz"
subpackages="$pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/kscreen.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# kscreen-kded-configtest is broken
	# kscreen-kded-osdtest hangs
	xvfb-run ctest --test-dir build --output-on-failure -E "kscreen-kded-(config|osd)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6c6c17066b320c4480534adfa0c39fb69d2b89e00b033613fd61a4d9ed6df112a750eef4b95301a42eddd4cbaec9e9e8bb77ffdc2dc9e377b07709afc67f0588  kscreen-5.27.7.tar.xz
"
