# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=khelpcenter
pkgver=23.04.3
pkgrel=1
pkgdesc="Application to show KDE Applications' documentation"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://userbase.kde.org/KHelpCenter"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	grantlee-dev
	karchive-dev
	kbookmarks-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdoctools-dev
	khtml-dev
	ki18n-dev
	kinit-dev
	kservice-dev
	kwindowsystem-dev
	libxml2-dev
	qt5-qtbase-dev
	samurai
	xapian-core-dev
	"
_repo_url="https://invent.kde.org/system/khelpcenter.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/khelpcenter-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
14cd545000de0eda4c3294da9f263bd56bf38d351baa830911795527a557964ff06ad382c21ff00d5d04427bd7d866f21369ee5375088006fc86292b0e4572a5  khelpcenter-23.04.3.tar.xz
"
