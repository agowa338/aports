# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=apt
pkgver=2.7.3
pkgrel=1
pkgdesc="APT package management tool"
url="https://salsa.debian.org/apt-team/apt"
arch="all"
license="GPL-2.0-or-later"
makedepends="
	bzip2-dev
	db-dev
	cmake
	dpkg-dev
	eudev-dev
	gettext-dev
	gnutls-dev
	libgcrypt-dev
	lz4-dev
	samurai
	triehash
	xxhash-dev
	xz-dev
	zlib-dev
	zstd-dev
	"
checkdepends="gtest-dev"
subpackages="
	$pkgname-dev
	$pkgname-libs
	"
source="https://salsa.debian.org/apt-team/apt/-/archive/$pkgver/apt-$pkgver.tar.bz2"
options="!check" # todo

build() {
	cmake -B build -G Ninja -Wno-dev \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DWITH_DOC=OFF \
		-DUSE_NLS=ON \
		-DWITH_TESTS="$(want_check && echo ON || echo OFF)"
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# libraries only
	cd "$pkgdir"
	rm -r usr/bin usr/libexec usr/share var etc
}

sha512sums="
32b90bd4df97c805232df0c956003cec374d75a5b0e4cbcfed372698003a02f2342676c4aef8720c0caec63dad174f6b0c4e13d771eee97fc5c2cb09d48b14c7  apt-2.7.3.tar.bz2
"
