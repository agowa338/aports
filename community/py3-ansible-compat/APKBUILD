# Contributor: Sean McAvoy <seanmcavoy@gmail.com>
# Maintainer: Sean McAvoy <seanmcavoy@gmail.com>
pkgname=py3-ansible-compat
pkgver=4.1.5
pkgrel=0
pkgdesc="functions that help interacting with various versions of Ansible"
url="https://github.com/ansible/ansible-compat"
arch="noarch"
license="MIT"
depends="
	python3
	py3-jsonschema
	py3-packaging
	py3-subprocess-tee
	py3-yaml
	"
makedepends="
	ansible-core
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
subpackages="$pkgname-pyc"
checkdepends="py3-pytest py3-flaky py3-pytest-mock"
#subpackages="$pkgname-doc $pkgname-pyc"
source="ansible-compat-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/a/ansible-compat/ansible-compat-$pkgver.tar.gz"
builddir="$srcdir/ansible-compat-$pkgver"
# the tests don't clean up after themselves, and fail if something is left in
# /tmp, and every release they add even more broken tests like this
options="!check"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	# FIXME: bad tests leave around state and fail on builders
	# on subsequent runs
	PYTHONPATH="$builddir/build/lib:$PYTHONPATH" pytest -v \
		-k "not test_runtime_install_role and not test_install_galaxy_role"
}

package() {
	python3 -m installer --destdir="$pkgdir" --compile-bytecode 0 \
		.dist/*.whl
}

sha512sums="
6da0bab56237a6447462ed9ef528711eb4c998de2732ec021e8257d2baf6cbb1e1435707440b4c0d122fb4f938c855693bcd555e147613f230105e3af74c00a6  ansible-compat-4.1.5.tar.gz
"
