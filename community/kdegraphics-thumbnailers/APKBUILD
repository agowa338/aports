# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdegraphics-thumbnailers
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
pkgdesc="Thumbnailers for various graphics file formats"
url="https://www.kde.org/applications/graphics/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	kdegraphics-mobipocket-dev
	kio-dev
	libkdcraw-dev
	libkexiv2-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/graphics/kdegraphics-thumbnailers.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdegraphics-thumbnailers-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
8fe5be6b16d58cd9f87966424499230ef15168cf022214d33cbcde6411617da051a4be4a4bbd12ef954cca4e3b0f7116b9e2409c1e9ea9ae9a376e17da8145e2  kdegraphics-thumbnailers-23.04.3.tar.xz
"
