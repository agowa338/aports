# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=grantlee-editor
pkgver=23.04.3
pkgrel=1
pkgdesc="Utilities and tools to manage themes in KDE PIM applications "
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	akonadi-mime-dev
	extra-cmake-modules
	grantleetheme-dev
	karchive-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kimap-dev
	knewstuff-dev
	kpimtextedit-dev
	ktexteditor-dev
	kxmlgui-dev
	libkleo-dev
	messagelib-dev
	pimcommon-dev
	qgpgme
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	syntax-highlighting-dev
	"
_repo_url="https://invent.kde.org/pim/grantlee-editor.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/grantlee-editor-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
4c1ecf77259741296c081387c53d42e28c1a34bcf959e0810e4bef3b5c561fe6d3c05f95b7305cf787ba7d3612e52afc00130e1bf577ae6967c299e00225a5d9  grantlee-editor-23.04.3.tar.xz
"
