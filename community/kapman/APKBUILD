# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kapman
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://kde.org/applications/games/org.kde.kapman"
pkgdesc="A clone of the well known game Pac-Man"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/kapman.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kapman-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e5ab132904acd28ce586dd0feb786ba94afc8726bcea4575fee83664d9c76db8bcb9708189322aafb5d95c9fd4dbc4e71e83ad459265d593dc1dd8ea3cf27454  kapman-23.04.3.tar.xz
"
