# Contributor: Oliver Smith <ollieparanoid@postmarketos.org>
# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Do not change arch to noarch, until this bug is resolved:
# https://gitlab.alpinelinux.org/alpine/abuild/-/issues/10022
pkgname=calamares-extensions
pkgver=1.3.0
pkgrel=1
# armhf blocked by qt5-qtdeclarative
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> calamares
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://github.com/calamares/calamares-extensions"
pkgdesc="Calamares Branding and Module Examples"
license="GPL-3.0-or-later"
makedepends="
	calamares-dev
	qt5-qtsvg-dev
	qt5-qtdeclarative-dev
	"
source="
	calamares-extensions-$pkgver.tar.gz::https://github.com/calamares/calamares-extensions/archive/refs/tags/v$pkgver.tar.gz
	0001-add-usermod-command-about-default-username.patch
	0002-change-username-before-password.patch
	0003-custom-username-also-changes-the-fullname-comment.patch
	"
options="!check" # has no tests

# Modules and brandings to build:
# https://github.com/calamares/calamares-extensions/tree/calamares/modules
# https://github.com/calamares/calamares-extensions/tree/calamares/branding
_modules="
	mobile
	"
_brandings="
	default-mobile
	"

for i in $_modules; do
	subpackages="$pkgname-mod-$i:_module $subpackages"
done
for i in $_brandings; do
	subpackages="$pkgname-brand-$i:_branding $subpackages"
done

# Check if one module/branding is enabled
# $1: name of module/branding
# $2: either $_modules or $_brandings
is_enabled() {
	local i
	for i in $2; do
		[ "$i" = "$1" ] && return 0
	done
	return 1
}

# Check if string $1 is in CMakeLists.txt and comment it out
comment_out() {
	sed -i "s~$1~#&~g" "$builddir/CMakeLists.txt"
}

prepare() {
	default_prepare

	local i

	msg "disabled modules:"
	cd "$builddir/modules"
	for i in *; do
		if ! [ -d "$i" ] || is_enabled "$i" "$_modules"; then
			continue
		fi
		echo " - $i"
		comment_out "calamares_add_module_subdirectory( modules/$i "
	done

	msg "disabled brandings:"
	cd "$builddir/branding"
	for i in *; do
		if ! [ -d "$i" ] || is_enabled "$i" "$_brandings"; then
			continue
		fi
		echo " - $i"
		comment_out "calamares_add_branding_subdirectory( branding/$i "
	done
}

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

_module() {
	local module=${subpkgname##calamares-extensions-mod-}
	local path="usr/lib/calamares/modules"

	mkdir -p "$subpkgdir/$path"
	mv "$pkgdir/$path/$module" "$subpkgdir/$path/$module"
}

_branding() {
	local branding=${subpkgname##calamares-extensions-brand-}
	local path="usr/share/calamares/branding"

	mkdir -p "$subpkgdir/$path"
	mv "$pkgdir/$path/$branding" "$subpkgdir/$path/$branding"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9edee20b7ea98724a8443a3cbcb70ffbe241b989e58931683687323815e0ffc38fda55720186e70f33d6b626d5f2475cb9a2d6bbd932840a27d0c66f2c6b3978  calamares-extensions-1.3.0.tar.gz
9fea30cd2dbbddb7b91f79cf171244eef0e2010831f9f0d7f06a2cb7d9ca689b8446020b0c26631bfbce473dd66f7dc4f0e4ac35ed15c80563364e4cd4db75b1  0001-add-usermod-command-about-default-username.patch
80fe2f86f981efdac094abeb193bada1d9007b22ee4c71345b82e521da7155341f2666a98c0705066eee66800d398cb9d10b54f19d841d79622489c017d563e4  0002-change-username-before-password.patch
413b836915ebedf9f418d3fc3710c162bfb5e6623d393a23cb5319a03483ffbf1173e322bd2662348dbfe758b5ae8bd0bf289985a75373919bb0b7bb0c18a1d0  0003-custom-username-also-changes-the-fullname-comment.patch
"
