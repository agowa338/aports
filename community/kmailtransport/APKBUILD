# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmailtransport
pkgver=23.04.3
pkgrel=1
pkgdesc="Manage mail transport"
# armhf blocked by extra-cmake-modules
# ppc64le and s390x blocked by libkgapi
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	akonadi-dev
	akonadi-mime-dev
	kcmutils-dev
	kconfigwidgets-dev
	ki18n-dev
	kio-dev
	kmime-dev
	ksmtp-dev
	kwallet-dev
	libkgapi-dev
	qtkeychain-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/kmailtransport.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmailtransport-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="!check" # Requires OpenGL and running dbus

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
f3c5e8422cea7e2620a1fbb9df951640d4b1a94e0b2dab0a3972d14291538b8ec3a06cf5e828994710b06683b8616e32fa4936ecfd54094e35d785b262e4989c  kmailtransport-23.04.3.tar.xz
"
