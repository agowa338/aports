# Maintainer: Donoban <donoban@riseup.net>
pkgname=pdfjs
pkgver=3.9.179
pkgrel=0
pkgdesc="A general-purpose, web standards-based platform for parsing and rendering PDFs."
url="https://mozilla.github.io/pdf.js/"
arch="noarch"
license="Apache-2.0"
source="https://github.com/mozilla/pdf.js/releases/download/v$pkgver/pdfjs-$pkgver-legacy-dist.zip"
options="!check" # No tests
subpackages="$pkgname-dbg"

prepare() {
	default_prepare

	#disable bundled fonts
	sed -i "s|\"../web/standard_fonts/\",|null,|" web/viewer.js
}

package() {
	# Remove bundled fonts and weird pdf included
	rm "$srcdir/web/compressed.tracemonkey-pldi-09.pdf"
	rm -fr "$srcdir/web/cmaps"
	rm -fr "$srcdir/web/standard_fonts"

	mkdir -p "$pkgdir/usr/share/pdf.js"
	cp -R "$srcdir"/build "$pkgdir"/usr/share/pdf.js
	cp -R "$srcdir"/web "$pkgdir"/usr/share/pdf.js
}

dbg() {
	amove /usr/share/pdf.js/*/*.js.map
	amove /usr/share/pdf.js/*/debugger*
}

sha512sums="
002395b7747104f1f515155c09871438afc773ba2680ae4de174078f15cb210f4613337f49ba82f62a4ac73cfc70d81f5a03459c36243802fcce7962e2b5a2ad  pdfjs-3.9.179-legacy-dist.zip
"
