# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kirigami-gallery
pkgver=23.04.3
pkgrel=1
arch="all !armhf" # armhf blocked by kirigami2 -> qt5-qtdeclarative
url="https://kde.org/applications/development/org.kde.kirigami2.gallery"
pkgdesc="Gallery application built using Kirigami"
license="LGPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kirigami2-dev
	kitemmodels-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/kirigami-gallery.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kirigami-gallery-$pkgver.tar.xz"
subpackages="$pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 $_repo_url .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1e8892f1469fdfee269acbedd37134c8da9c296f399a9cd356ac59755592c269f10279aa9e005993b23086fd4defde166746804b91611b3753b4886ce1c8e3d7  kirigami-gallery-23.04.3.tar.xz
"
