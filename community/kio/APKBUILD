# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kio
pkgver=5.108.0
pkgrel=2
pkgdesc="Resource and network access abstraction"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only AND LGPL-2.1-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends_dev="
	karchive-dev
	kbookmarks-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kitemviews-dev
	kjobwidgets-dev
	knotifications-dev
	kservice-dev
	kwallet-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtscript-dev
	solid-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	kdoctools-dev
	libxml2-dev
	libxslt-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/frameworks/kio.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kio-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang $pkgname-kwallet"
options="!check" # Fails due to requiring physical devices not normally available and test 14 hangs

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kio.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

kwallet() {
	pkgdesc="$pkgname KWallet integration"

	amove usr/lib/qt5/plugins/kf5/kiod/kpasswdserver.so
}

sha512sums="
846ae1931793b97dde436396ced9341e072cae18869e4eb755c4024dee7c725dde9d02f44779705d744823bfe898f08b6139b070737617d09122fbc6c72c8154  kio-5.108.0.tar.xz
"
