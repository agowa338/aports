# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=akregator
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://apps.kde.org/akregator/"
pkgdesc="RSS Feed Reader"
license="GPL-2.0-or-later AND LGPL-2.0-or-later"
makedepends="
	akonadi-mime-dev
	extra-cmake-modules
	grantlee-dev
	grantleetheme-dev
	kcmutils-dev
	kcrash-dev
	kdoctools-dev
	kiconthemes-dev
	knotifications-dev
	knotifyconfig-dev
	kontactinterface-dev
	kparts-dev
	kpimtextedit-dev
	ktexteditor-dev
	kxmlgui-dev
	libkdepim-dev
	libkleo-dev
	messagelib-dev
	pimcommon-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	samurai
	syndication-dev
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/akregator.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/akregator-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1eaa63e8e3867720f930278b1a4366c30dca4384712499bfb588d8e31cfb49692dcdaf2cbd958fa4e5ffeddd0f6a58beb9579a5f3a3abc02a7409a26b4caa9e4  akregator-23.04.3.tar.xz
"
