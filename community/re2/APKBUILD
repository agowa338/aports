# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=re2
pkgver=2023.08.01
_pkgver=${pkgver//./-}
pkgrel=0
pkgdesc="Efficient, principled regular expression library"
url="https://github.com/google/re2"
arch="all"
license="BSD-3-Clause"
makedepends="
	abseil-cpp-dev
	cmake
	icu-dev
	samurai
	"
checkdepends="
	benchmark-dev
	gtest-dev
	"
subpackages="$pkgname-dev"
provides="libre2=$pkgver-r$pkgrel"
source="$pkgname-$pkgver.tar.gz::https://github.com/google/re2/archive/$_pkgver.tar.gz"
builddir="$srcdir"/$pkgname-$_pkgver

build() {
	export CXXFLAGS="$CXXFLAGS -O2 -flto=auto"
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DBUILD_SHARED_LIBS=ON \
		-DRE2_USE_ICU=ON \
		-DRE2_BUILD_TESTING="$(want_check && echo ON || echo OFF)"
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
33703f87dbb359e481c46fd97977d420a9f0f3d4b378b8a0ecdec60d3c7b237d941b37780101f229c5e4785898a208963616a390d6c01d849c455716eae85106  re2-2023.08.01.tar.gz
"
