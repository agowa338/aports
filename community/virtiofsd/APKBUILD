# Maintainer:
pkgname=virtiofsd
pkgver=1.7.0
pkgrel=1
pkgdesc="virtio-fs vhost-user device daemon"
url="https://gitlab.com/virtio-fs/virtiofsd"
# fails to build on 32-bit
arch="all !x86 !armhf !armv7"
license="Apache-2.0 AND BSD-3-Clause"
makedepends="
	cargo
	cargo-auditable
	libcap-ng-dev
	libseccomp-dev
	"
source="https://gitlab.com/virtio-fs/virtiofsd/-/archive/v$pkgver/virtiofsd-v$pkgver.tar.bz2
	lfs64.patch
	"
builddir="$srcdir/virtiofsd-v$pkgver"
options="net"

# qemu doesn't ship this anymore, split to this project
provides="qemu-virtiofsd=$pkgver-r$pkgrel"
replaces="qemu-virtiofsd"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

check() {
	cargo test --frozen --release
}

package() {
	install -Dm755 target/release/virtiofsd \
		-t "$pkgdir"/usr/lib/qemu/
	install -Dm644 50-qemu-virtiofsd.json \
		-t "$pkgdir"/usr/share/qemu/vhost-user/
}

sha512sums="
989d2775626727a94436a28d39f808511f9c3fbc5ac6aa5b1cf08f8580343070cecd7e212dcbfb02cb5aa8f23f4c5c96995c7d2257b24a9b5ca7dd6ae2048fa1  virtiofsd-v1.7.0.tar.bz2
a7a92300da2457a74aef7538c79a3ed5833fa511a399cfe4165fe6af16586de34c93570dd0b14e7325accb22cd9e5695dc32a0e425c7daf1b3b13214004f1fc3  lfs64.patch
"
