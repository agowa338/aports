# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-mobile
pkgver=5.27.7
pkgrel=1
pkgdesc="Modules providing phone functionality for Plasma"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.plasma-mobile.org/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="
	breeze-icons
	dbus-x11
	kactivities
	kpipewire
	maliit-keyboard
	plasma-nano
	plasma-nm-mobile
	plasma-pa
	plasma-settings
	plasma-workspace
	qqc2-breeze-style
	qt5-qtquickcontrols2
	"
makedepends="
	extra-cmake-modules
	kactivities-dev
	kauth-dev
	kbookmarks-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kitemviews-dev
	kjobwidgets-dev
	knotifications-dev
	kpackage-dev
	kpeople-dev
	kservice-dev
	kwayland-dev
	kwidgetsaddons-dev
	kwin-dev
	kwindowsystem-dev
	kxmlgui-dev
	libphonenumber-dev
	modemmanager-qt-dev
	networkmanager-qt-dev
	plasma-framework-dev
	plasma-workspace-dev
	qt5-qtdeclarative-dev
	samurai
	solid-dev
	telepathy-qt-dev
	"

provides="plasma-phone-components=$pkgver-r$pkgrel" # For backwards compatibility
replaces="plasma-phone-components" # For backwards compatibility

subpackages="$pkgname-lang"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-mobile.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-mobile-$pkgver.tar.xz"
options="!check" # No tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-mobile.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ee383d00dc0567605def6510467f69f6b78d63970ab6f527f69263d63729f63259a285e151844396f36a9904734dbf0fb0e35725b2d61a422ffbd6aa4a8a6a0c  plasma-mobile-5.27.7.tar.xz
"
