# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdialog
pkgver=23.04.3
pkgrel=1
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/"
pkgdesc="A utility for displaying dialog boxes from shell scripts"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kguiaddons-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	ktextwidgets-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/kdialog.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdialog-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
5c40ee102f7251a660f30f1d84b485ce259a003e533d710e2a760fc167a1ccb804b59b5f1de68f8e7c8619080c8b99046745da9cf1c4e30fe626c8c5c9295ab5  kdialog-23.04.3.tar.xz
"
