# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-iosevka
pkgver=26.0.2
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
695284f9bd00efbdf0ba41b31d41f144bb6817778055afb0464561b88bdd0555ede017670c69399c56fd36ab792cae60c4040c27e7cce13aa54aca1419a3e784  super-ttc-iosevka-26.0.2.zip
650be2e72ea3841ecca3262dbfaa8b080955d2b38797ab36d53da99b1758b17dabd22790c9ffd0e7734a7c193649c15503bbc5ff110a148be7c3efa72476b3e8  super-ttc-iosevka-aile-26.0.2.zip
4f130654252b3bc5f43dcc859ca6521d3b4ab2f40f48e0fe6332d91c7abecfbd74c130ca23606a1e4910233b61dd79548d0877a08fa48d30f7158858921f0806  super-ttc-iosevka-slab-26.0.2.zip
53e7c0daf0fc7e5fb95dfe048e4d4db1170047e247e6b241777079f85024e18ba4ea56a9d683a86f69b1ea40c4d5d9eb2a80def3f78612a7a70a104dc6c838ba  super-ttc-iosevka-curly-26.0.2.zip
dd13d26ecc21c2c7572bec670fed5b6ffc6c370456bac39d326475057d530d983fa81764b4d3e222bef575ca2a1c5528b833039c7e85aac850f6426d7806021b  super-ttc-iosevka-curly-slab-26.0.2.zip
"
