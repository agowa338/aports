# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-remotecontrollers
pkgver=5.27.7
pkgrel=1
pkgdesc="Translate various input device events into keyboard and pointer events"
url="https://invent.kde.org/plasma-bigscreen/plasma-remotecontrollers"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="BSD-2-Clause AND (GPL-2.0-only OR GPL-3.0-only) AND CC0-1.0"
# For some reason there is no dep on so:libcec but it crashes without it
depends="libcec"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kdeclarative-dev
	ki18n-dev
	knotifications-dev
	kpackage-dev
	kwindowsystem-dev
	libcec-dev
	libevdev-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtwayland-dev
	samurai
	solid-dev
	"
case "$CARCH" in
	ppc64le) ;;
	*) makedepends="$makedepends xwiimote-dev" ;;
esac

subpackages="$pkgname-lang"
case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/plasma-remotecontrollers.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-remotecontrollers-$pkgver.tar.xz
	uinput.conf
	"
options="!check" # No tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-remotecontrollers.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	install -Dm644 \
		"$srcdir"/uinput.conf \
		-t "$pkgdir"/etc/modules-load.d
}

sha512sums="
1af749f2d9cb6c385ebdd73a9f8e10e209d93f9ceca99d5002fa7fa4fea5cee3f84907ce32cf2dacdf5bb27c090b67bb04272df08c8a333768f83bd09ef8f83f  plasma-remotecontrollers-5.27.7.tar.xz
a9b069ed121ffeee887e0583d8cb46035ecf1fa90a26a4ecb3aa11ff03178b2b08621f6676db6b2350f290694c04aabcf36f2ce3e0813a76dde9a33555edb112  uinput.conf
"
