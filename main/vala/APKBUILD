# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Contributor: Natanael Copa <ncopa@alpinelinux.org>
# Maintainer: Rasmus Thomsen <oss@cogitri.dev>
pkgname=vala
pkgver=0.56.10
pkgrel=0
pkgdesc="Compiler for the GObject type system"
url="https://wiki.gnome.org/Projects/Vala"
arch="all"
license="LGPL-2.0-or-later"
subpackages="$pkgname-devhelp $pkgname-dbg $pkgname-doc"
depends="glib-dev"
makedepends="libxslt-dev bash flex bison gobject-introspection-dev graphviz-dev"
checkdepends="dbus-x11"
source="https://download.gnome.org/sources/vala/${pkgver%.*}/vala-$pkgver.tar.xz"

prepare() {
	default_prepare
	# 2 failures
	sed -i "/constants\/member-access/d" \
		tests/Makefile.in
}

build() {
	CFLAGS="$CFLAGS -O2 -flto=auto" \
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
0ec6589067214e76fa0656b178ddf2784451e02b9d5bcfac3f35372c239d00aaf36af317924c72732620f97d4c32845a10d4b86be5ba0e30a6efa5283200fe02  vala-0.56.10.tar.xz
"
