# Maintainer: Ariadne Conill <ariadne@dereferenced.org>
pkgname=pkgconf
pkgver=2.0.1
pkgrel=0
pkgdesc="development framework configuration tools"
url="https://gitea.treehouse.systems/ariadne/pkgconf"
arch="all"
license="ISC"
replaces="pkgconfig"
provides="pkgconfig=1"
checkdepends="kyua atf"
subpackages="$pkgname-doc $pkgname-dev"
source="https://distfiles.ariadne.space/pkgconf/pkgconf-$pkgver.tar.xz
	"

# secfixes:
#   1.9.4-r0:
#     - CVE-2023-24056

build() {
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--with-pkg-config-dir=/usr/local/lib/pkgconfig:/usr/local/share/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	ln -s pkgconf "$pkgdir"/usr/bin/pkg-config
}

dev() {
	default_dev

	# Move pkg-config back to main package (default_dev implicitly moves
	# files /usr/bin/*-config to -dev).
	mv "$subpkgdir"/usr/bin/pkg-config "$pkgdir"/usr/bin/

	mkdir -p "$pkgdir"/usr/share/aclocal/
	mv "$subpkgdir"/usr/share/aclocal/pkg.m4 "$pkgdir"/usr/share/aclocal/
}

sha512sums="
1f6e6e421e6e6228aad5cbd834016c8657a59e3bbb5929de4b62377a79cb4e4a52e339f305378b80e6309701bbc0490179e0ead7bdee9da6d7c78565d7b80bc8  pkgconf-2.0.1.tar.xz
"
