# Contributor: Ben Pye <ben@curlybracket.co.uk>
# Maintainer: Ben Pye <ben@curlybracket.co.uk>
pkgname=libunwind
pkgver=1.7.1
pkgrel=0
pkgdesc="Portable and efficient C programming interface (API) to determine the call-chain of a program"
url="https://www.nongnu.org/libunwind/"
arch="all"
license="MIT"
options="!check" # v1.4.0 12 tests failing - https://github.com/libunwind/libunwind/issues/164
depends_dev="libucontext-dev"
makedepends_build="autoconf automake libtool"
makedepends_host="$depends_dev linux-headers xz-dev"
makedepends="$makedepends_build $makedepends_host"
subpackages="$pkgname-dbg $pkgname-static $pkgname-dev $pkgname-doc"
source="https://github.com/libunwind/libunwind/releases/download/v$pkgver/libunwind-1.7.1.tar.gz
	force-enable-man.patch
	fix-libunwind-pc-in.patch
	"

build() {
	# shellcheck disable=2046
	LDFLAGS="$LDFLAGS -lucontext" CFLAGS="$CFLAGS -fno-stack-protector" \
	./configure \
		--build="$CBUILD" \
		--host="$CHOST" \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-cxx-exceptions \
		$(want_check || echo --disable-tests)
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
372fd57663c46788104632e7f721871afe33a2244cc45c38d754e5d10b6692a41dc0d24aa673df94cc6790b4fac0d3341bc4499872ef461e106e8887dadf3830  libunwind-1.7.1.tar.gz
0a6de92aba6624298df4173e075947112369d6d59c0db8ba58d59611f799ba0f192515d3493304de8125a11b67529517e5d830f05ff5d4fc86b8cca6ca2872b8  force-enable-man.patch
ab5d44e9d3aaf32e3119fe79389e5dfcdc859c78cfda8400e54ee29fd1cdf04b99e2686caf18ab0b76ac94f861861d69a5cf740d46967af9c630095485523f1c  fix-libunwind-pc-in.patch
"
