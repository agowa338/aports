# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Celeste <cielesti@protonmail.com>
pkgname=ssldump
pkgver=1.7
pkgrel=1
pkgdesc="SSLv3/TLS network protocol analyzer"
url="https://github.com/adulau/ssldump"
arch="all"
license="BSD-4-Clause"
subpackages="$pkgname-doc"
options="!check"  # no tests provided
makedepends="autoconf automake libnet-dev json-c-dev musl-fts-dev libpcap-dev openssl-dev>3"
source="https://github.com/adulau/ssldump/archive/v$pkgver/ssldump-$pkgver.tar.gz"

prepare() {
	default_prepare
	./autogen.sh
}

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--datarootdir=/usr/share \
		--docdir=/usr/share/doc \
		--localstatedir=/var
	make
}

package() {
	make install sbindir="$pkgdir/usr/sbin" mandir="$pkgdir/usr/share/man" docdir="$pkgdir/usr/share/doc"
	install -Dm644 COPYRIGHT -t "$pkgdir"/usr/share/licenses/$pkgname
}

sha512sums="
1836d8cc968f006f7c503c5537594e2e47d35a8cfbb4c4de0eb3154e7f20b60d965589b4cdf6adf46584c8b55300bc12e186ddfc9641b8039c2851bec463c461  ssldump-1.7.tar.gz
"
