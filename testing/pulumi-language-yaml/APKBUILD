# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=pulumi-language-yaml
pkgver=1.2.0
pkgrel=0
pkgdesc="Infrastructure as Code SDK (YAML language provider)"
url="https://pulumi.com/"
# blocked by pulumi
arch="x86_64 aarch64"
license="Apache-2.0"
depends="pulumi"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi-yaml/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/pulumi-yaml-$pkgver"
# Requires PULUMI_ACCESS_TOKEN for tests to be run
options="!check"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOFLAGS="$GOFLAGS -modcacherw"

build() {
	go build -v \
		-ldflags "-X github.com/pulumi/pulumi-yaml/pkg/version.Version=v$pkgver" \
		-o $pkgname \
		./cmd/pulumi-language-yaml/
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
0c5a6b9d925ee4045e19739d6519a4a89b98cf450108cef354503d77e1f4bda840c4a08d69ca4d6ec4b9316f9650d8e52b365977b2cce06ed50abf941b899054  pulumi-language-yaml-1.2.0.tar.gz
"
