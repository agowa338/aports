# Contributor: Thomas Kienlen <kommander@laposte.net>
# Maintainer: Thomas Kienlen <kommander@laposte.net>
pkgname=scaleway-cli
pkgver=2.19.0
pkgrel=0
pkgdesc="Command-line client for Scaleway Cloud"
url="https://www.scaleway.com/en/cli"
arch="all !x86 !armv7 !armhf" # tests are failing for x86, armv7, armhf
license="Apache-2.0"
makedepends="go"
source="$pkgname-$pkgver.tar.gz::https://github.com/scaleway/scaleway-cli/archive/refs/tags/v$pkgver.tar.gz"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
options="chmod-clean"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -o scw ./cmd/scw/main.go
	PATH=. scw autocomplete script shell=bash > bashcomp
	PATH=. scw autocomplete script shell=fish > fishcomp
	PATH=. scw autocomplete script shell=zsh > zshcomp
}

check() {
	go test -v ./...
}

package() {
	install -Dm755 scw "$pkgdir"/usr/bin/scw

	install -Dm644 bashcomp "$pkgdir"/usr/share/bash-completion/completions/scw
	install -Dm644 fishcomp "$pkgdir"/usr/share/fish/vendor_completions.d/scw.fish
	install -Dm644 zshcomp "$pkgdir"/usr/share/zsh/site-functions/_scw

}

sha512sums="
b8d6f8e5a27149811d0f6913b8241aca2a609f89b1818ba94ba3a1b1b143fc238df19ebf75e9b8146e53e90c2867be0d954235172e0836cfa454de344b3f1872  scaleway-cli-2.19.0.tar.gz
"
