# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ruff
pkgver=0.0.284
pkgrel=0
pkgdesc="Extremely fast Python linter"
url="https://github.com/charliermarsh/ruff"
# ppc64le, s390x, riscv64: ring
# x86, armhf, armv7: fails tests on 32-bit
arch="all !x86 !armhf !armv7 !ppc64le !s390x !riscv64"
license="MIT"
makedepends="py3-gpep517 py3-maturin cargo py3-installer"
subpackages="
	$pkgname-pyc
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	"
source="https://github.com/charliermarsh/ruff/archive/v$pkgver/ruff-$pkgver.tar.gz"
# net: cargo
options="net"

export CARGO_PROFILE_RELEASE_OPT_LEVEL=2

prepare() {
	default_prepare

	# shadow git repo for tests
	git init -q

	cargo fetch --locked
}

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--config-json '{"build-args": "--frozen"}' \
		--output-fd 3 3>&1 >&2

	./target/release/ruff --generate-shell-completion bash > $pkgname.bash
	./target/release/ruff --generate-shell-completion fish > $pkgname.fish
	./target/release/ruff --generate-shell-completion zsh > $pkgname.zsh

	# Update ruff.schema.json as the pre-built one is generated
	# using the '--all-features' Cargo flag which we don't pass.
	cargo dev generate-all
}

check() {
	unset CI_PROJECT_DIR

	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl

	install -Dm644 $pkgname.bash \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 $pkgname.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/$pkgname.fish
	install -Dm644 $pkgname.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
2ba28bc9f2606b5e1e7d192c148204dd9b35fcaea5b69c0b8788a10fb67d4a1f22d12edb1855f233194a3f984312c8c8aee61defde4dc942494ac9194aed92e6  ruff-0.0.284.tar.gz
"
