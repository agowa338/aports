# Maintainer:
pkgname=m17n-lib
pkgver=1.8.3
pkgrel=1
pkgdesc="gnu M-text multilingualization library"
url="https://www.nongnu.org/m17n/"
arch="all"
license="LGPL-2.1-or-later"
depends="m17n-db~$pkgver"
depends_dev="
	m17n-db-dev~$pkgver
	$pkgname-tools=$pkgver-r$pkgrel
	"
makedepends="
	anthy-dev
	fontconfig-dev
	freetype-dev
	fribidi-dev
	gettext-dev
	libthai-dev
	libx11-dev
	libxaw-dev
	libxft-dev
	libxml2-dev
	libxt-dev
	"
subpackages="
	$pkgname-dev
	libm17n-core
	libm17n-flt
	$pkgname-tools
	"
source="http://download.savannah.gnu.org/releases/m17n/m17n-lib-$pkgver.tar.gz"

build() {
	export CFLAGS="$CFLAGS -flto=auto $(pkg-config --cflags glib-2.0)"
	export LIBS="-lintl"
	./configure \
		--host=$CHOST \
		--build=$CBUILD \
		--prefix=/usr \
		--disable-dependency-tracking \
		--disable-static
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

core() {
	pkgdesc="$pkgdesc (-core lib)"

	amove usr/lib/libm17n-core.so.*
}

flt() {
	pkgdesc="$pkgdesc (-flt lib)"

	amove usr/lib/libm17n-flt.so.*
}

tools() {
	pkgdesc="$pkgdesc (extra tools)"

	amove usr/bin
}

sha512sums="
3f162fded40b3c84fc112c265ebda7a11cf8c4010ec3506a3b20c5f014210914068d47e5338862e86e4d978ef89c6d09938ecf92b0e71a95f29a5f3681dd28b6  m17n-lib-1.8.3.tar.gz
"
