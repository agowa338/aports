# Maintainer: Cowington Post <cowingtonpost@gmail.com>
pkgname=calibre
pkgver=6.24.0
pkgrel=0
pkgdesc="Ebook management application"
# qt6-webengine
arch="aarch64 x86_64"
url="https://calibre-ebook.com"
license="GPL-3.0-or-later"
depends="
	font-liberation
	libwmf
	mtdev
	optipng
	poppler
	py3-apsw
	py3-beautifulsoup4
	py3-css-parser
	py3-cssselect
	py3-dateutil
	py3-dnspython
	py3-feedparser
	py3-fonttools
	py3-html2text
	py3-html5-parser
	py3-html5lib
	py3-jeepney
	py3-lxml
	py3-markdown
	py3-mechanize
	py3-msgpack
	py3-netifaces
	py3-pillow
	py3-psutil
	py3-pycryptodome
	py3-pygments
	py3-pyqt6-webengine
	py3-regex
	py3-zeroconf
	qt6-qtimageformats
	qt6-qtsvg
	qt6-qtwebengine
	udisks2
	"
makedepends="
	cmake
	hunspell-dev
	hyphen-dev
	libmtp-dev
	libstemmer-dev
	libusb-dev
	podofo-dev
	py3-pyqt-builder
	py3-pyqt6-sip
	py3-sip
	python3-dev
	qt6-qtbase-dev
	uchardet-dev
	xdg-utils
	"
subpackages="
	$pkgname-pyc
	$pkgname-doc
	$pkgname-bash-completion
	$pkgname-zsh-completion
	"
source="https://download.calibre-ebook.com/$pkgver/calibre-$pkgver.tar.xz"

export LANG="en_US.UTF-8"

prepare() {
	default_prepare

	rm -f resources/calibre-portable.*
}

build() {
	python3 setup.py build
	python3 setup.py iso639
	python3 setup.py iso3166
	python3 setup.py liberation_fonts --system-liberation_fonts --path-to-liberation_fonts /usr/share/fonts/liberation
	python3 setup.py mathjax
	python3 setup.py gui
}

check() {
	python3 -m unittest discover
}

package() {
	# needed for zsh
	mkdir -p "$pkgdir"/usr/share/zsh/site-functions

	python3 setup.py install \
		--staging-root="$pkgdir"/usr \
		--system-plugins-location=/usr/share/calibre/system-plugins

	cp -a man-pages/ "$pkgdir"/usr/share/man

	rm -r "$pkgdir"/usr/share/calibre/rapydscript/

	python3 -m compileall -fq "$pkgdir"/usr
}

sha512sums="
90612438c863f32204a53611f03ea330ac839c3feb179978f34d01f9a90b13d249e9d0bca24e37159731c653b543f80fa573053251c4cf3ab1d7af00ddb5cc66  calibre-6.24.0.tar.xz
"
