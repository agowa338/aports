# Contributor: omni <omni+alpine@hack.org>
# Maintainer: omni <omni+alpine@hack.org>
pkgname=snowflake
pkgver=2.6.0
pkgrel=0
pkgdesc="Pluggable Transport for Tor using WebRTC, inspired by Flashproxy"
url="https://snowflake.torproject.org/"
license="BSD-3-Clause"
arch="all !riscv64" # ftbfs
makedepends="go"
options="!check" # no test files
subpackages="$pkgname-doc"
source="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/archive/v$pkgver/snowflake-v$pkgver.tar.gz"
builddir="$srcdir/$pkgname-v$pkgver"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	cd "$builddir"/client
	go build -v .

	cd "$builddir"/proxy
	go build -v .
}

package() {
	install -Dm0755 client/client "$pkgdir"/usr/bin/"$pkgname"-client
	install -Dm0755 proxy/proxy "$pkgdir"/usr/bin/"$pkgname"-proxy
	install -Dm0644 doc/snowflake-*.1 -t "$pkgdir"/usr/share/man/man1
}

sha512sums="
e55dcff21f59b61b2d5cbcd4d4133d5df0493a0e1ecb5847b7337e72cc99a24eece79f1624231493f049745adf2b9706edc5add705206b66c7acefba154a146d  snowflake-v2.6.0.tar.gz
"
