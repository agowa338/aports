# Contributor: Grigory Kirillov <txgk@bk.ru>
# Maintainer:
pkgname=fheroes2
pkgver=1.0.6
pkgrel=1
pkgdesc="Recreation of Heroes of Might and Magic II game engine"
url="https://github.com/ihhub/fheroes2"
arch="all"
license="GPL-2.0-only"
options="!check" # no test suite
makedepends="
	gettext
	gnu-libiconv
	sed
	sdl2-dev
	sdl2_image-dev
	sdl2_mixer-dev
	zlib-dev
	"
subpackages="$pkgname-lang"
source="https://github.com/ihhub/fheroes2/archive/$pkgver/fheroes2-$pkgver.tar.gz"
install="$pkgname.post-install"
langdir="/usr/share/$pkgname/files/lang"

build() {
	make FHEROES2_WITH_IMAGE=1 FHEROES2_DATA=/usr/share/fheroes2
	# Use GNU iconv instead of musl-based one, because this project uses
	# TRANSLIT iconv extension which is not present in musl implementation.
	make -C files/lang ICONV=gnu-iconv
}

package() {
	install -Dm755 src/dist/fheroes2 -t "$pkgdir"/usr/bin/
	install -Dm644 script/packaging/common/fheroes2.desktop -t "$pkgdir"/usr/share/applications/
	install -Dm644 files/data/*.h2d -t "$pkgdir"/usr/share/"$pkgname"/files/data/
	install -Dm644 files/lang/*.mo -t "$pkgdir"/usr/share/"$pkgname"/files/lang/
	install -Dm755 script/demo/download_demo_version.sh -t "$pkgdir"/usr/share/"$pkgname"/
	install -Dm755 script/homm2/extract_homm2_resources.sh -t "$pkgdir"/usr/share/"$pkgname"/
	install -Dm644 src/resources/fheroes2.png -t "$pkgdir"/usr/share/pixmaps/
	install -dm755 "$pkgdir"/usr/share/fheroes2/data
	install -dm755 "$pkgdir"/usr/share/fheroes2/maps
}

sha512sums="
c42b2ded44b7d6e112de3e919044b6b7d69161cc8cf08198df88da42c7f77b2dcf9df9ea7b453def66902e85f42047c0375b093c8d2d91517acb42d1804e621b  fheroes2-1.0.6.tar.gz
"
