# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=irust
pkgver=1.70.0
pkgrel=0
pkgdesc="Cross-platform Rust REPL"
url="https://github.com/sigmaSd/IRust"
arch="all"
license="MIT"
depends="cargo rust"
makedepends="cargo cargo-auditable"
source="https://github.com/sigmaSd/IRust/archive/v$pkgver/irust-$pkgver.tar.gz
	getrandom-0.2.10.patch
	"
builddir="$srcdir/IRust-$pkgver"
options="!check"  # FIXME: tests don't run on CI (require TTY)

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	cargo test --frozen
}

package() {
	install -D -m755 target/release/$pkgname -t "$pkgdir"/usr/bin/
}

sha512sums="
d383d531e1b027b6d042fa8d6377bc6b76eb478c9abfaee4165ef4e1c8d556d27345a011487677c27bea6ac8fa2a470cc618fce2a00cde12476f016352c8e02b  irust-1.70.0.tar.gz
fd90d461cf517e8019af5ff6ac074c95903c5340c86d2ab8ff51bcabd29e022514b84f9d68a498499654e47b6cb04857639f5be110990daf1c313e64e9ce7b6e  getrandom-0.2.10.patch
"
