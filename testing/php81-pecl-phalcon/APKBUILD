# Contributor: Valery Kartel <valery.kartel@gmail.com>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php81-pecl-phalcon
_extname=phalcon
pkgver=5.2.3
pkgrel=0
pkgdesc="High performance, full-stack PHP 8.1 framework delivered as a C extension"
url="https://phalcon.io/"
arch="all"
license="BSD-3-Clause"
_phpv=81
_php=php$_phpv
depends="
	$_php-curl
	$_php-fileinfo
	$_php-gettext
	$_php-mbstring
	$_php-openssl
	$_php-pdo
	$_php-session
	$_php-pecl-psr
	"
makedepends="$_php-dev"
source="php-$_extname-$pkgver.tgz::https://pecl.php.net/get/$_extname-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize$_phpv
	./configure --prefix=/usr --with-php-config=/usr/bin/php-config$_phpv
	make
}

check() {
	# no tests provided
	$_php -d extension=modules/$_extname.so --ri $_extname
}

package() {
	make INSTALL_ROOT="$pkgdir" install

	local _confdir="$pkgdir"/etc/$_php/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/$_extname.ini
}

sha512sums="
143bacd3ca35bac7fbbc7d243e26ebffa6acc9ff70a952173846aa37d45bd242095432ce557ba3bda1ea73623ad1fa96b8e12cbbbbdaf393a2cd0ec9060ff734  php-phalcon-5.2.3.tgz
"
