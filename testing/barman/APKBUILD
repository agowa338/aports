# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=barman
pkgver=3.6.0
pkgrel=0
pkgdesc="Backup and recovery manager for PostgreSQL"
url="http://www.pgbarman.org"
arch="noarch"
license="GPL-3.0-or-later"
depends="python3 rsync postgresql-client py3-argcomplete py3-dateutil py3-psycopg2 py3-boto3"
makedepends="py3-setuptools"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-pyc"
pkgusers="barman"
pkggroups="barman"
install="$pkgname.pre-install"
options="!check" #pytest does not start and I don't know why
checkdepends="py3-pytest-timeout py3-mock py3-pytest-runner py3-pip py3-mock"
source="$pkgname-$pkgver.tar.gz::https://github.com/EnterpriseDB/barman/releases/download/release/$pkgver/barman-$pkgver.tar.gz"

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"

	install -Dm0644 ./scripts/barman.bash_completion \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -d -o $pkgusers -g $pkggroups -m 755 "$pkgdir"/var/log/$pkgname

	cd doc
	install -Dm0644 barman.conf -t  "$pkgdir/etc"
}

sha512sums="
02d8d58e5d59d073badb9338042e5d58a103e1a24508128c726c9dbdf8b77f5bc49472ac05f1e156d78106b7464c98229dcabf6be98773fa260ff6b90b804ad6  barman-3.6.0.tar.gz
"
