# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: Andy Postnikov <apostnikov@gmail.com>
pkgname=php8-brotli
_extname=brotli
pkgver=0.14.0
pkgrel=0
pkgdesc="Brotli Extension for PHP 8.0"
url="https://github.com/kjdev/php-ext-brotli"
arch="all"
license="MIT"
_phpv=8
_php=php$_phpv
depends="$_php-common"
makedepends="$_php-dev $_php-pecl-apcu brotli-dev"
checkdepends="$_php-cgi"
source="php-pecl-$_extname-$pkgver.tgz::https://pecl.php.net/get/brotli-$pkgver.tgz"
builddir="$srcdir/$_extname-$pkgver"

build() {
	phpize$_phpv
	./configure \
		--prefix=/usr \
		--with-php-config=php-config$_phpv \
		--with-libbrotli # Use system lib
	make
}

check() {
	local _modules=/usr/lib/$_php/modules
	make NO_INTERACTION=1 REPORT_EXIT_STATUS=1 SKIP_ONLINE_TESTS=1 test \
		TEST_PHP_CGI_EXECUTABLE=/usr/bin/php-cgi$_phpv \
		PHP_TEST_SHARED_EXTENSIONS=" \
		-d extension=$_modules/apcu.so \
		-d extension=modules/$_extname.so" TESTS=--show-diff
}

package() {
	make INSTALL_ROOT="$pkgdir" install
	local _confdir="$pkgdir"/etc/$_php/conf.d
	install -d $_confdir
	echo "extension=$_extname" > $_confdir/50_$_extname.ini
}

sha512sums="
fea89d953441b874509cfd4a94f7e9c947e2d3759593e156ba6bcea0072212d60f120769f21f8ee779f9f9f0acd71b204997e21fb4d39d87682f4fbc5149c1ba  php-pecl-brotli-0.14.0.tgz
"
