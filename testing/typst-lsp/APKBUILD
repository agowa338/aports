# Maintainer: Lauren N. Liberda <lauren@selfisekai.rocks>
pkgname=typst-lsp
pkgver=0.9.0
pkgrel=0
pkgdesc="Language server for typst"
url="https://github.com/nvarner/typst-lsp"
# typst, rust-analyzer
arch="aarch64 ppc64le x86_64"
license="MIT"
depends="rust-analyzer"
makedepends="
	cargo
	cargo-auditable
	openssl-dev
	"
source="
	https://github.com/nvarner/typst-lsp/archive/refs/tags/v$pkgver/typst-lsp-$pkgver.tar.gz
	yes-openssl.patch
	"
options="net !check" # no tests

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen
}

package() {
	install -Dm755 target/release/typst-lsp -t "$pkgdir"/usr/bin/
}

sha512sums="
6ac5d23dd81282a6ec09c2d2ff039b9d6365034006839910e38c6bbc345d7b5b7a765dd89fc449b995948d4799915426d3a4c1dc460e87058c24e8e04aa113e8  typst-lsp-0.9.0.tar.gz
afabbbe8b2c78708bfa2009c06ebd839b6d20de995516da12930929a0b2c3a30ecdcfe295d86fd19313c28cdb735c04ae7e326cb9f178317208e476953256d11  yes-openssl.patch
"
